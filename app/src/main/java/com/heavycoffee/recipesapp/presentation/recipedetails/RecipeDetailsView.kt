package com.heavycoffee.recipesapp.presentation.recipedetails

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.heavycoffee.recipesapp.entity.RecipeDetails

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface RecipeDetailsView : MvpView {
    fun onLoadDetails(details: RecipeDetails)
    fun onShowFullScreenImg(position: Int, images: List<String>)
    fun onShowToast(strResId: Int)
}