package com.heavycoffee.recipesapp.presentation.recipedetails

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.entity.app.DataWrapper
import com.heavycoffee.recipesapp.model.interactor.RecipeInteractor
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */
@InjectViewState
class RecipeDetailsPresenter @Inject constructor(
    private val recipeInteractor: RecipeInteractor,
    private val router: Router,
    private val uuid: DataWrapper<String>
): MvpPresenter<RecipeDetailsView>() {
    private val compositeDisposable = CompositeDisposable()
    private var isFullScreenImageOpen = false

    override fun onFirstViewAttach() {
        getDetails(uuid.value)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun getDetails(uuid: String) {
        val single = recipeInteractor
            .getRecipeDetails(uuid)
            .subscribe({
                viewState.onLoadDetails(it)
            }, {
                viewState.onShowToast(R.string.error_loading)
            })

        compositeDisposable.add(single)
    }

    fun openFullScreenImg(position: Int, images: List<String>) {
        isFullScreenImageOpen = true
        viewState.onShowFullScreenImg(position, images)
    }

    fun onCloseFullScreenImg() {
        isFullScreenImageOpen = false
    }

    fun onBackPressed() = router.exit()
}