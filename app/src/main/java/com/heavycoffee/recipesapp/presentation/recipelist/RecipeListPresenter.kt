package com.heavycoffee.recipesapp.presentation.recipelist

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.Screens
import com.heavycoffee.recipesapp.entity.app.SortField
import com.heavycoffee.recipesapp.model.interactor.RecipeInteractor
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */

@InjectViewState
class RecipeListPresenter @Inject constructor(
    private val recipeInteractor: RecipeInteractor,
    private val router: Router
): MvpPresenter<RecipeListView>() {
    private val compositeDisposable = CompositeDisposable()
    private var isSearchOpen = false
    private var query: String = ""

    override fun onFirstViewAttach() {
        if (isSearchOpen) {
            viewState.onRestoreSearchState(query)
        }
        getRecipes(query, SortField.NONE, true)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun getRecipes(
        search: String,
        sortEnum: SortField,
        prgoress: Boolean = false
    ) {
        viewState.onShowProgress(prgoress)
        val single = recipeInteractor
            .getRecipeList(search, sortEnum)
            .subscribe({
                viewState.onLoadItems(it)
                viewState.onShowProgress(false)
                viewState.onShowNoDataMessage(it.isEmpty())
            }, {
                it.printStackTrace()
                viewState.onShowProgress(false)
                viewState.onShowToast(R.string.error_loading)
            })
        compositeDisposable.add(single)
    }

    fun search(query: String) {
        this.query = query
        getRecipes(query, SortField.NONE)
    }

    fun sortByName() {
        getRecipes(query, SortField.NAME)
    }

    fun sortByLastUpdate() {
        getRecipes(query, SortField.LAST_UPDATE)
    }

    fun openSearch() {
        isSearchOpen = true
        viewState.onSearchOpen()
    }

    fun closeSearch() {
        isSearchOpen = false
        viewState.onSearchClose()
    }

    fun refresh() {
        getRecipes(query, SortField.NONE)
    }

    fun openRecipeDetails(uuid: String) {
        router.navigateTo(Screens.RecipeDetailsScreen(uuid))
    }

    fun onBackPressed() = router.exit()
}