package com.heavycoffee.recipesapp.presentation.recipelist

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.heavycoffee.recipesapp.entity.RecipeListItem

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface RecipeListView : MvpView {
    fun onLoadItems(list: List<RecipeListItem>)
    fun onSearchOpen()
    fun onSearchClose()
    fun onRestoreSearchState(query: String)
    fun onShowNoDataMessage(visible: Boolean)
    fun onShowProgress(visible: Boolean)
    fun onShowToast(strResId: Int)
}