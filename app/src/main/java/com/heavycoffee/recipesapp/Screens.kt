package com.heavycoffee.recipesapp

import com.heavycoffee.recipesapp.entity.RecipeDetails
import com.heavycoffee.recipesapp.ui.fragment.recipedetails.TextContentFragment
import com.heavycoffee.recipesapp.ui.fragment.recipedetails.RecipeDetailsFragment
import com.heavycoffee.recipesapp.ui.fragment.recipedetails.RecipeSimilarFragment
import com.heavycoffee.recipesapp.ui.fragment.recipelist.RecipeListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */
object Screens {
    object RecipeListScreen: SupportAppScreen() {
        override fun getFragment() = RecipeListFragment.getInstance()
    }

    data class RecipeDetailsScreen(
        val recipeUUID: String
    ): SupportAppScreen() {
        override fun getFragment() = RecipeDetailsFragment.getInstance(recipeUUID)
    }

    data class RecipeSimilarListScreen(
        val similar: RecipeDetails
    ): SupportAppScreen() {
        override fun getFragment() = RecipeSimilarFragment.getInstance(similar)
    }

    data class RecipeDescriptionScreen(
        val description: String?
    ): SupportAppScreen() {
        override fun getFragment() = TextContentFragment.getInstance(description)
    }

    data class RecipeInstructionScreen(
        val instruction: String?
    ): SupportAppScreen() {
        override fun getFragment() = TextContentFragment.getInstance(instruction)
    }
}