package com.heavycoffee.recipesapp

import android.app.Application
import com.heavycoffee.recipesapp.di.DI
import com.heavycoffee.recipesapp.di.module.AppModule
import com.heavycoffee.recipesapp.di.module.ServerModule
import toothpick.Toothpick
import toothpick.configuration.Configuration

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        initToothpick()
        initAppScope()
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }
    }

    private fun initAppScope() {
        Toothpick.openScope(DI.APP_SCOPE)
            .installModules(
                AppModule(this),
                ServerModule()
            )
    }
}