package com.heavycoffee.recipesapp.di

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 05.07.2019.
 */
object DI {
    const val APP_SCOPE = "app_scope"
}