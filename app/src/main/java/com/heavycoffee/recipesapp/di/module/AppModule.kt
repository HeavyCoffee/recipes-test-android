package com.heavycoffee.recipesapp.di.module

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.heavycoffee.recipesapp.di.ServerPath
import com.heavycoffee.recipesapp.model.data.db.AppDatabase
import com.heavycoffee.recipesapp.model.data.server.RecipeApi
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 29.06.2019.
 */
class AppModule(context: Context) : Module() {
    init {
        bind(Context::class.java).toInstance(context)
        bind(String::class.java).withName(ServerPath::class.java).toInstance("https://test.kode-t.ru")

        //navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        //db
        val database = Room
            .databaseBuilder(context, AppDatabase::class.java, "app_database")
            .fallbackToDestructiveMigration()
            .build()

        bind(AppDatabase::class.java).toInstance(database)
    }
}