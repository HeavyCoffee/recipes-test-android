package com.heavycoffee.recipesapp.di.provider

import com.heavycoffee.recipesapp.di.ServerPath
import com.heavycoffee.recipesapp.model.data.server.RecipeApi
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Provider

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 03.07.2019.
 */
class ApiProvider @Inject constructor(
    val okHttpClient: OkHttpClient,
    @ServerPath val baseUrl: String
): Provider<RecipeApi> {
    override fun get() =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .callbackExecutor(Executors.newSingleThreadExecutor())
            .build()
            .create(RecipeApi::class.java)
}