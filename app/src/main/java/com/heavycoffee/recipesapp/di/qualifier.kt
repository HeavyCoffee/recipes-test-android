package com.heavycoffee.recipesapp.di

import javax.inject.Qualifier

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 07.07.2019.
 */
@Qualifier
annotation class ServerPath