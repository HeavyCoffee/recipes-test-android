package com.heavycoffee.recipesapp.di.module

import com.heavycoffee.recipesapp.di.provider.ApiProvider
import com.heavycoffee.recipesapp.di.provider.OkhttpProvider
import com.heavycoffee.recipesapp.model.data.server.RecipeApi
import com.heavycoffee.recipesapp.model.interactor.RecipeInteractor
import com.heavycoffee.recipesapp.model.repository.RecipeRepository
import okhttp3.OkHttpClient
import toothpick.config.Module

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 03.07.2019.
 */
class ServerModule : Module() {
    init {
        bind(OkHttpClient::class.java).toProvider(OkhttpProvider::class.java).providesSingletonInScope()
        bind(RecipeApi::class.java).toProvider(ApiProvider::class.java).providesSingletonInScope()

        bind(RecipeRepository::class.java)
        bind(RecipeInteractor::class.java)
    }
}