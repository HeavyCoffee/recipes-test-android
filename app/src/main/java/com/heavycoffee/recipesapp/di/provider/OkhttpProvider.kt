package com.heavycoffee.recipesapp.di.provider

import com.heavycoffee.recipesapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 03.07.2019.
 */
class OkhttpProvider @Inject constructor(

) : Provider<OkHttpClient> {
    private val TIMEOUT = 30L

    override fun get() = with(OkHttpClient.Builder()) {
        connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        readTimeout(TIMEOUT, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            addInterceptor(HttpLoggingInterceptor())
        }
        build()
    }
}