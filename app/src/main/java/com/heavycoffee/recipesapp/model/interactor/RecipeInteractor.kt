package com.heavycoffee.recipesapp.model.interactor

import com.heavycoffee.recipesapp.entity.RecipeListItem
import com.heavycoffee.recipesapp.entity.app.SortField
import com.heavycoffee.recipesapp.model.repository.RecipeRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 30.06.2019.
 */

class RecipeInteractor @Inject constructor(
    private val repository: RecipeRepository
) {
    fun getRecipeList(
        search: String,
        sortEnum: SortField = SortField.NONE
    ): Single<List<RecipeListItem>> {
        return when {
            search.isEmpty() &&
                    sortEnum == SortField.NONE -> repository.loadRecipeList()
            sortEnum == SortField.NONE -> repository.getRecipeList(search)
            sortEnum == SortField.NAME -> repository.getRecipeListSortByName(search)
            sortEnum == SortField.LAST_UPDATE -> repository.getRecipeListSortByLastUpdate(search)
            else -> repository.loadRecipeList()
        }
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getRecipeDetails(uuid: String) =
        repository
            .getRecipeDetails(uuid)
            .observeOn(AndroidSchedulers.mainThread())
}