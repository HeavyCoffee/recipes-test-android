package com.heavycoffee.recipesapp.model.repository

import com.heavycoffee.recipesapp.model.data.db.AppDatabase
import com.heavycoffee.recipesapp.model.data.server.RecipeApi
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 30.06.2019.
 */
class RecipeRepository @Inject constructor(
    val api: RecipeApi,
    val appDatabase: AppDatabase
) {
    fun loadRecipeList() =
        api.getRecipeList()
            .map {
                appDatabase
                    .recipeListItemDao
                    .insert(it.list)
                return@map it.list
            }
            .onErrorResumeNext(getRecipeList(""))

    fun getRecipeListSortByLastUpdate(search: String) =
        appDatabase
            .recipeListItemDao
            .getRecipeListByLastUpdate(search)
            .subscribeOn(Schedulers.io())

    fun getRecipeListSortByName(search: String) =
        appDatabase
            .recipeListItemDao
            .getRecipeListSortByName(search)
            .subscribeOn(Schedulers.io())

    fun getRecipeList(search: String) =
        appDatabase
            .recipeListItemDao
            .getRecipeList(search)
            .subscribeOn(Schedulers.io())

    fun getRecipeDetails(uuid: String) =
        api.getRecipeDetails(uuid)
            .map {
                return@map it.recipe
            }
}