package com.heavycoffee.recipesapp.model.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.heavycoffee.recipesapp.entity.RecipeListItem
import com.heavycoffee.recipesapp.model.data.typeconverter.StringListConverter

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 01.07.2019.
 */
@Database(entities = [RecipeListItem::class], version = 1, exportSchema = false)
@TypeConverters(StringListConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract val recipeListItemDao: RecipeListItemDao
}