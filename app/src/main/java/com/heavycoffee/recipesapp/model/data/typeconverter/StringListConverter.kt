package com.heavycoffee.recipesapp.model.data.typeconverter

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 07.07.2019.
 */
class StringListConverter {
    companion object {
        @TypeConverter
        @JvmStatic
        fun fromJsonString(string: String): List<String> {
            return Gson().fromJson(string, object : TypeToken<List<String>>() {}.type)
        }

        @TypeConverter
        @JvmStatic
        fun toJsonString(list: List<String>): String {
            return Gson().toJson(list)
        }
    }
}