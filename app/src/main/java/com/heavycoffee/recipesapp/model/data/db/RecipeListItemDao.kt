package com.heavycoffee.recipesapp.model.data.db

import androidx.room.*
import com.heavycoffee.recipesapp.entity.RecipeListItem
import io.reactivex.Single

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 01.07.2019.
 */
@Dao
interface RecipeListItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<RecipeListItem>)

    @Delete
    fun delete(list: List<RecipeListItem>)

    @Query("SELECT * FROM recipelistitem " +
            "WHERE name LIKE '%'||:search||'%' " +
            "OR description LIKE '%'||:search||'%' " +
            "OR instructions LIKE '%'||:search||'%'")
    fun getRecipeList(search: String): Single<List<RecipeListItem>>

    @Query("SELECT * FROM recipelistitem " +
            "WHERE name LIKE '%'||:search||'%' " +
            "OR description LIKE '%'||:search||'%' " +
            "OR instructions LIKE '%'||:search||'%' ORDER BY name ASC")
    fun getRecipeListSortByName(search: String): Single<List<RecipeListItem>>

    @Query("SELECT * FROM recipelistitem " +
            "WHERE name LIKE '%'||:search||'%' " +
            "OR description LIKE '%'||:search||'%' " +
            "OR instructions LIKE '%'||:search||'%' ORDER BY lastUpdated DESC")
    fun getRecipeListByLastUpdate(search: String): Single<List<RecipeListItem>>
}