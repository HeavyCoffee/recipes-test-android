package com.heavycoffee.recipesapp.model.data.server

import com.heavycoffee.recipesapp.entity.RecipeList
import com.heavycoffee.recipesapp.entity.RecipeObj
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 01.07.2019.
 */

interface RecipeApi {
    @GET("/recipes")
    fun getRecipeList(): Single<RecipeList>

    @GET("/recipes/{uuid}")
    fun getRecipeDetails(@Path("uuid") uuid: String): Single<RecipeObj>
}