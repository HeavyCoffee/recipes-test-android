package com.heavycoffee.recipesapp.ui.fragment.recipelist

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.entity.RecipeListItem
import com.heavycoffee.recipesapp.extension.hideKeyboard
import com.heavycoffee.recipesapp.presentation.recipelist.RecipeListPresenter
import com.heavycoffee.recipesapp.presentation.recipelist.RecipeListView
import com.heavycoffee.recipesapp.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_recipe_list.*
import kotlinx.android.synthetic.main.view_search.*

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 29.06.2019.
 */
class RecipeListFragment : BaseFragment(), RecipeListView {
    companion object {
        fun getInstance() = RecipeListFragment()
    }

    @InjectPresenter
    lateinit var presenter: RecipeListPresenter

    @ProvidePresenter
    fun providePresenter(): RecipeListPresenter =
        scope.getInstance(RecipeListPresenter::class.java)

    private val adapter: RecipeListAdapter by lazy {
        RecipeListAdapter { uuid ->
            presenter.openRecipeDetails(uuid)
        }
    }

    override var layoutRes = R.layout.fragment_recipe_list

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.apply {
            setTitle(R.string.app_name)
            inflateMenu(R.menu.toolbar_menu)
            setOnMenuItemClickListener {
                when(it.itemId) {
                    R.id.menuItemSortByName -> { presenter.sortByName() }
                    R.id.menuItemSortByUpdate -> { presenter.sortByLastUpdate() }
                    R.id.appBarSearch -> { presenter.openSearch() }
                }
                true
            }
        }

        rvList.layoutManager = LinearLayoutManager(context)
        rvList.adapter = adapter

        etSearchQuery.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                presenter.search(s.toString())
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        ibSearchClose.setOnClickListener { presenter.closeSearch() }
        srRefresh.setOnRefreshListener { presenter.refresh() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    override fun onLoadItems(list: List<RecipeListItem>) {
        srRefresh.isRefreshing = false
        adapter.dataset.clear()
        adapter.dataset.addAll(list)
        adapter.notifyDataSetChanged()
    }

    override fun onSearchClose() {
        searchView.isVisible = false
        etSearchQuery.text.clear()
        activity?.hideKeyboard()
    }

    override fun onSearchOpen() {
        searchView.isVisible = true
        etSearchQuery.requestFocus()
    }

    override fun onRestoreSearchState(query: String) {
        etSearchQuery.setText(query)
        onSearchOpen()
    }

    override fun onShowProgress(visible: Boolean) {
        progressBar.isVisible = visible
    }

    override fun onShowNoDataMessage(visible: Boolean) {
        noDataMessage.isVisible = visible
    }

    override fun onShowToast(strResId: Int) {
        Toast.makeText(context, strResId, Toast.LENGTH_SHORT).show()
    }
}