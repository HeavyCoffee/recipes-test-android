package com.heavycoffee.recipesapp.ui.fragment.recipedetails

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.entity.RecipeBrief
import kotlinx.android.synthetic.main.item_recipe_brief.view.*

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 14.07.2019.
 */
class SimilarAdapter(
    list: List<RecipeBrief>,
    private val onClickListener: (uuid: String) -> Unit
): RecyclerView.Adapter<SimilarAdapter.SimilarViewHolder>() {
    private var dataset: List<RecipeBrief> = listOf()

    init {
        dataset = list
        notifyDataSetChanged()
    }

    override fun getItemCount() = dataset.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimilarViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(
                R.layout.item_recipe_brief,
                parent,
                false
            )
        return SimilarViewHolder(view)
    }

    override fun onBindViewHolder(holder: SimilarViewHolder, position: Int) {
        holder.bind(dataset[position])
    }

    inner class SimilarViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(recipe: RecipeBrief) {
            itemView.rootLayout.setOnClickListener { onClickListener(recipe.uuid) }
            itemView.tvRecipeName.text = recipe.name
        }
    }
}