package com.heavycoffee.recipesapp.ui.fragment.recipedetails

import android.os.Bundle
import androidx.core.view.isVisible
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_text_content.*

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 14.07.2019.
 */
class TextContentFragment : BaseFragment() {
    companion object {
        fun getInstance(description: String?) = TextContentFragment().apply {
            arguments = Bundle().apply {
                putString(CONTENT_KEY, description)
            }
        }
    }
    private val CONTENT_KEY = "content_key"

    override var layoutRes = R.layout.fragment_text_content

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val textContent = arguments?.getString(CONTENT_KEY, "") ?: ""

        if (textContent.isNotEmpty()) {
            tvText.text = textContent
        } else {
            noDataMessage.isVisible = true
        }
    }
}