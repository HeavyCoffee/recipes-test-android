package com.heavycoffee.recipesapp.ui.common.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.squareup.picasso.Picasso

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 08.07.2019.
 */
class ImgPagerAdapter(
    private val onClickListener: (position: Int, images: List<String>) -> Unit
) : PagerAdapter() {
    private var images: ArrayList<String> = arrayListOf()

    fun setImages(images: List<String>) {
        this.images.addAll(images)
        notifyDataSetChanged()
    }

    override fun getCount() = images.size

    override fun isViewFromObject(view: View, `object`: Any) = view == `object`

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imgView = ImageView(container.context)
        imgView.setOnClickListener { onClickListener(position, images) }
        setImg(imgView, images[position])
        container.addView(imgView)
        return imgView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ImageView)
    }

    private fun setImg(imgView: ImageView, imgUrl: String) {
        Picasso.get()
            .load(imgUrl)
            .centerCrop()
            .fit()
            .into(imgView)
    }
}