package com.heavycoffee.recipesapp.ui.fragment.recipedetails

import android.os.Bundle
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.Screens
import com.heavycoffee.recipesapp.entity.RecipeDetails
import com.heavycoffee.recipesapp.entity.app.DataWrapper
import com.heavycoffee.recipesapp.presentation.recipedetails.RecipeDetailsPresenter
import com.heavycoffee.recipesapp.presentation.recipedetails.RecipeDetailsView
import com.heavycoffee.recipesapp.ui.common.adapter.FragmentAdapter
import com.heavycoffee.recipesapp.ui.common.adapter.ImgPagerAdapter
import com.heavycoffee.recipesapp.ui.fragment.BaseFragment
import com.squareup.picasso.Picasso
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.fragment_recipe_details.*
import toothpick.Scope
import toothpick.config.Module

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 29.06.2019.
 */
class RecipeDetailsFragment : BaseFragment(), RecipeDetailsView {
    companion object {
        fun getInstance(recipeUUID: String) = RecipeDetailsFragment().apply {
            arguments = Bundle().apply {
                putString(RECIPE_UUID, recipeUUID)
            }
        }
    }

    private val RECIPE_UUID = "recipe_uuid"
    @InjectPresenter
    lateinit var presenter: RecipeDetailsPresenter

    @ProvidePresenter
    fun providePresenter(): RecipeDetailsPresenter =
        scope.getInstance(RecipeDetailsPresenter::class.java)

    private val adapter: ImgPagerAdapter by lazy {
        ImgPagerAdapter { position, images ->
            presenter.openFullScreenImg(position, images)
        }
    }

    private val fragmentAdapter: FragmentAdapter by lazy {
        FragmentAdapter(childFragmentManager)
    }

    override var layoutRes = R.layout.fragment_recipe_details

    override fun installModules(scope: Scope) {
        super.installModules(scope)
        val uuid = arguments?.getString(RECIPE_UUID)

        scope.installModules(
            object : Module() {
                init {
                    bind(DataWrapper::class.java)
                        .toInstance(DataWrapper(uuid))
                }
            }
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
        initImgPager()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    override fun onLoadDetails(details: RecipeDetails) {
        setImages(details.images)
        toolbar.title = details.name
        initFragmentPager(details)
    }

    override fun onShowFullScreenImg(position: Int, images: List<String>) {
        StfalconImageViewer.Builder(context, images) { view, image ->
            Picasso.get().load(image).into(view)
        }
            .withStartPosition(position)
            .allowSwipeToDismiss(true)
            .withDismissListener { presenter.onCloseFullScreenImg() }
            .show()
    }

    override fun onShowToast(strResId: Int) {
        Toast.makeText(context, strResId, Toast.LENGTH_SHORT).show()
    }

    private fun setImages(images: List<String>) {
        adapter.setImages(images)
    }

    private fun initImgPager() {
        vpImgViewer.adapter = adapter
        vpImgViewer.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                tvPhotoPosition.text = getString(
                    R.string.recipe_details_image_counter,
                    position + 1,
                    adapter.count
                )
            }
            override fun onPageSelected(position: Int) {}
        })
    }

    private fun initFragmentPager(details: RecipeDetails) {
        viewPager.adapter = fragmentAdapter
        fragmentAdapter.fragments = listOf(
            Screens.RecipeDescriptionScreen(details.description).fragment,
            Screens.RecipeInstructionScreen(details.instructions).fragment,
            Screens.RecipeSimilarListScreen(details).fragment
        )
        fragmentAdapter.titles = listOf(
            getString(R.string.recipe_details_tab_description_title),
            getString(R.string.recipe_details_tab_instructions_title),
            getString(R.string.recipe_details_tab_similar_title)
        )
        fragmentAdapter.notifyDataSetChanged()
        tabLayout.setupWithViewPager(viewPager)
    }
}