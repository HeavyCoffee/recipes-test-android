package com.heavycoffee.recipesapp.ui.fragment.recipedetails

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.Screens
import com.heavycoffee.recipesapp.entity.RecipeBrief
import com.heavycoffee.recipesapp.entity.RecipeDetails
import com.heavycoffee.recipesapp.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_recipe_similar.*
import ru.terrakok.cicerone.Router
import toothpick.Toothpick
import javax.inject.Inject

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 14.07.2019.
 */
class RecipeSimilarFragment : BaseFragment(){
    companion object {
        fun getInstance(recipe: RecipeDetails) = RecipeSimilarFragment().apply {
            arguments = Bundle().apply {
                putSerializable(RECIPE, recipe)
            }
        }
    }

    private val RECIPE = "recipe_key"

    @Inject
    lateinit var router: Router

    override var layoutRes = R.layout.fragment_recipe_similar

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Toothpick.inject(this, scope)
        arguments?.let {
            val similar = (arguments?.getSerializable(RECIPE) as? RecipeDetails)?.similar ?: listOf()
            if (similar.isNotEmpty()) {
                initRecipeBriefList(similar)
            } else {
                noDataMessage.isVisible = true
            }
        }
    }

    private fun initRecipeBriefList(list: List<RecipeBrief>) {
        rvList.layoutManager = LinearLayoutManager(context)
        rvList.adapter = SimilarAdapter(list) { uuid ->
            router.navigateTo(Screens.RecipeDetailsScreen(uuid))
        }
    }
}