package com.heavycoffee.recipesapp.ui.fragment.recipelist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.heavycoffee.recipesapp.R
import com.heavycoffee.recipesapp.entity.RecipeListItem
import com.heavycoffee.recipesapp.picasso.RoundedCornersTransformation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_recipe.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 07.07.2019.
 */
class RecipeListAdapter(
    private val onClickListener: (String) -> Unit
) : RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder>(){
    val dataset: ArrayList<RecipeListItem> = arrayListOf()

    override fun getItemCount() = dataset.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(
                R.layout.item_recipe,
                parent,
                false
            )
        return RecipeViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.bind(dataset[position])
    }

    private fun setImg(imgView: ImageView, imgUrl: String) {
        Picasso.get()
            .load(imgUrl)
            .fit()
            .transform(RoundedCornersTransformation(25))
            .into(imgView)
    }

    private fun getFormattedDate(sec: Int): String {
        val date = Date().apply {
            time = sec * 1000L
        }
        return SimpleDateFormat(
            "dd/MM/yyyy",
            Locale.getDefault()
        ).format(date)
    }

    inner class RecipeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(recipeItem: RecipeListItem) {
            val context = itemView.context

            if (recipeItem.images.isNotEmpty()) {
                setImg(itemView.ivRecipeImg, recipeItem.images.first())
            }

            itemView.tvRecipeName.text = recipeItem.name
            itemView.tvRecipeDescription.text = recipeItem.description
            itemView.tvLastDateUpdate.text = context.getString(
                R.string.recipe_list_item_last_date_update,
                getFormattedDate(recipeItem.lastUpdated)
            )
            itemView.rootLayout.setOnClickListener { onClickListener(recipeItem.uuid) }
        }
    }
}