package com.heavycoffee.recipesapp.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.heavycoffee.recipesapp.di.DI
import com.heavycoffee.recipesapp.extension.objectName
import toothpick.Scope
import toothpick.Toothpick

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 29.06.2019.
 */
abstract class BaseFragment : MvpAppCompatFragment() {
    private val STATE_SCOPE_KEY = "state_scope_name"
    abstract var layoutRes: Int

    private val parentScopeName: String by lazy {
        (parentFragment as? BaseFragment)?.fragmentScopeName ?: DI.APP_SCOPE
    }
    private lateinit var fragmentScopeName: String
    var instanceStateSaved = false
    lateinit var scope: Scope

    override fun onCreate(savedInstanceState: Bundle?) {
        fragmentScopeName = savedInstanceState?.getString(STATE_SCOPE_KEY) ?: objectName()

        if (Toothpick.isScopeOpen(this)) {
            scope = Toothpick.openScope(fragmentScopeName)
        } else {
            scope = Toothpick.openScopes(parentScopeName, fragmentScopeName)
            installModules(scope)
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onResume() {
        super.onResume()
        instanceStateSaved = false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isNeedScopeClose()) {
            Toothpick.closeScope(scope.name)
        }
    }

    private fun isFragmentRemoving(): Boolean {
        return (isRemoving && !instanceStateSaved) ||
                (parentFragment as? BaseFragment)?.isFragmentRemoving() ?: false
    }

    private fun isNeedScopeClose(): Boolean {
        return when {
            activity?.isChangingConfigurations == true -> false
            activity?.isFinishing == true -> true
            else -> isFragmentRemoving()
        }
    }

    protected open fun installModules(scope: Scope) {}

    open fun onBackPressed() {}
}