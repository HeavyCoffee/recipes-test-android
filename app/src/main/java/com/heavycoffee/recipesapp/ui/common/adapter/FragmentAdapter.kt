package com.heavycoffee.recipesapp.ui.common.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 14.07.2019.
 */
class FragmentAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    var fragments: List<Fragment> = listOf()
    var titles: List<String> = listOf()

    override fun getCount() = fragments.size

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }
}