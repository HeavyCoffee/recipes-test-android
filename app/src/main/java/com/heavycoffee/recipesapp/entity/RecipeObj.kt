package com.heavycoffee.recipesapp.entity

import com.google.gson.annotations.SerializedName

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 08.07.2019.
 */
data class RecipeObj(
    @SerializedName("recipe") val recipe: RecipeDetails
)