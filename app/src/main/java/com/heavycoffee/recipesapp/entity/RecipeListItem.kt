package com.heavycoffee.recipesapp.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.heavycoffee.recipesapp.model.data.typeconverter.StringListConverter

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */
@Entity
data class RecipeListItem (
    @PrimaryKey
    @SerializedName("uuid") val uuid: String,
    @SerializedName("name") val name: String,
    @SerializedName("images") val images: List<String>,
    @SerializedName("lastUpdated") val lastUpdated: Int,
    @SerializedName("description") val description: String?,
    @SerializedName("instructions") val instructions: String,
    @SerializedName("difficulty") val difficulty: Int
)