package com.heavycoffee.recipesapp.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 18.06.2019.
 */
data class RecipeDetails (
    @SerializedName("uuid") val uuid: String,
    @SerializedName("name") val name: String,
    @SerializedName("images") val images: List<String>,
    @SerializedName("lastUpdated") val lastUpdated: Int,
    @SerializedName("description") val description: String,
    @SerializedName("instructions") val instructions: String,
    @SerializedName("difficulty") val difficulty: Int,
    @SerializedName("similar") val similar: List<RecipeBrief>
) : Serializable