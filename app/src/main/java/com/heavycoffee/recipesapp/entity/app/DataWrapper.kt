package com.heavycoffee.recipesapp.entity.app

import java.io.Serializable

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 14.07.2019.
 */
data class DataWrapper <out T> (
    val value: T
): Serializable