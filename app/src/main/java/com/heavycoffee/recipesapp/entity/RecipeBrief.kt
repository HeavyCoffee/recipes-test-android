package com.heavycoffee.recipesapp.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 19.06.2019.
 */

data class RecipeBrief (
    @SerializedName("uuid") val uuid: String,
    @SerializedName("name") val name: String
) : Serializable