package com.heavycoffee.recipesapp.entity.app

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 11.07.2019.
 */
enum class SortField {
    NAME,
    LAST_UPDATE,
    NONE
}