package com.heavycoffee.recipesapp.entity

import com.google.gson.annotations.SerializedName

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 07.07.2019.
 */
data class RecipeList(
    @SerializedName("recipes") val list: List<RecipeListItem>
)