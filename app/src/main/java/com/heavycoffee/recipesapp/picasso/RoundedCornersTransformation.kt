package com.heavycoffee.recipesapp.picasso

import android.graphics.*
import com.squareup.picasso.Transformation

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 07.07.2019.
 */
class RoundedCornersTransformation(private val radius: Int) : Transformation {

    override fun key() = "rounded_transformation"

    override fun transform(source: Bitmap): Bitmap {
        val width = source.width
        val height = source.height

        val bitmap = Bitmap.createBitmap(
            width,
            height,
            Bitmap.Config.ARGB_8888
        )

        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.isAntiAlias = true
        paint.shader = BitmapShader(
            source,
            Shader.TileMode.CLAMP,
            Shader.TileMode.CLAMP
        )

        canvas.drawRoundRect(
            RectF(0f, 0f, width.toFloat(), height.toFloat()),
            radius.toFloat(),
            radius.toFloat(),
            paint
        )
        source.recycle()

        return bitmap
    }
}