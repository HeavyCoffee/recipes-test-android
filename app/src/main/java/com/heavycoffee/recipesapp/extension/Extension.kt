package com.heavycoffee.recipesapp.extension

/**
 * @author Vladimir Anisimov (HeavyCoffee) on 06.07.2019.
 */
fun Any.objectName() = "${javaClass.simpleName}_${hashCode()}"